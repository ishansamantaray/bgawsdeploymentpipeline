package aws.spring.boot.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {"aws.spring.boot.demo.controller"})
public class SpringBootWebApplication {
	
public static void main(String[] args) {
	SpringApplication.run(SpringBootWebApplication.class, args);
}
}
