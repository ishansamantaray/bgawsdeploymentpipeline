# ecs.tf

resource "aws_ecs_cluster" "main" {
  name = "GITLAB-TERRAFORM_TEST-CLUSTER-01"
}

data "template_file" "task-def" {
  template = file("./scripts/task-definition.json.tpl")

  vars = {
    app_image      = var.app_image
    app_port       = var.app_port
    fargate_cpu    = var.fargate_cpu
    fargate_memory = var.fargate_memory
    aws_region     = var.aws_region
  }
}

resource "aws_ecs_task_definition" "app" {
  family                   = "GITLAB-TERRAFORM_TEST-CLUSTER-APP-TASK"
  execution_role_arn       = "arn:aws:iam::975403186615:role/ecsTaskExecutionRole"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.task-def.rendered
}

resource "aws_ecs_service" "main" {
  name            = "GITLAB-TERRAFORM_TEST-CLUSTER-SERVICE"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"
  
  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = aws_subnet.public.*.id
    assign_public_ip = true
  }
  
  load_balancer {
    target_group_arn = aws_lb_target_group.app.id
    container_name   = "GITLAB-TERRAFORM_TEST-CLUSTER-APP-TASK"
    container_port   = var.app_port
  }
  
   depends_on = [aws_lb_listener.front_end,aws_lb_listener.front_end_2]
   
}

  
