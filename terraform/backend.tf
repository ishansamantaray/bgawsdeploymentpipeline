terraform {
  backend "s3" {
    bucket  = "terraformawspipelinebucket"
    key       = "terraform.tfstate"
    region   = "us-east-2"
    encrypt  =  true
  }
}

