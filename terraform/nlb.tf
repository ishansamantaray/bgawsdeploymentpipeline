# nlb.tf

resource "aws_lb" "main" {
  name            = "tf-load-balancer"
  internal           = true
  load_balancer_type = "network"
  subnets         = aws_subnet.public.*.id
}

resource "aws_api_gateway_vpc_link" "tfvpc" {
  name        = "tf-vpc-link"
  target_arns = ["${aws_lb.main.arn}"]
}

resource "aws_lb_target_group" "app" {
  name        = "tf-target-group"
  port        = 9000
  protocol    = "TCP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
}

# Redirect all traffic from the NLB to the target group
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.main.id
  port              = var.app_port
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.app.id
    type             = "forward"
  }
 } 
# Redirect all traffic from the NLB to the target group
resource "aws_lb_listener" "front_end_2" {
  load_balancer_arn = aws_lb.main.id
  port              = 80
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.app.id
    type             = "forward"
  }
}