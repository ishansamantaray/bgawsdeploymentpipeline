#apigateway.tf

resource "aws_api_gateway_rest_api" "tf-gw-rest-api" {
  name = "tf-gw-rest-api"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "tf-gw-resource" {
  rest_api_id = aws_api_gateway_rest_api.tf-gw-rest-api.id
  parent_id   = aws_api_gateway_rest_api.tf-gw-rest-api.root_resource_id
  path_part   = "index"
}

resource "aws_api_gateway_method" "tf-gw-method" {
  rest_api_id   = aws_api_gateway_rest_api.tf-gw-rest-api.id
  resource_id   = aws_api_gateway_resource.tf-gw-resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "tf-gw-integration" {
  rest_api_id = aws_api_gateway_rest_api.tf-gw-rest-api.id
  resource_id = aws_api_gateway_resource.tf-gw-resource.id
  http_method = aws_api_gateway_method.tf-gw-method.http_method
  type                    = "HTTP"
  uri                     = "http://tf-load-balancer-3b013c53cdac8b0e.elb.us-east-2.amazonaws.com/index"
  integration_http_method = "GET"
  connection_type = "VPC_LINK"
  connection_id   = aws_api_gateway_vpc_link.tfvpc.id
}

resource "aws_api_gateway_deployment" "gw-deployment" {
  depends_on  = [aws_api_gateway_integration.tf-gw-integration]
  rest_api_id = aws_api_gateway_rest_api.tf-gw-rest-api.id
  stage_name  = "intermediate"
}

resource "aws_api_gateway_stage" "gw-stage" {
  stage_name    = "test"
  rest_api_id   = aws_api_gateway_rest_api.tf-gw-rest-api.id
  deployment_id = aws_api_gateway_deployment.gw-deployment.id
}

resource "aws_api_gateway_deployment" "tf-gw-deployment" {
  depends_on = [aws_api_gateway_integration.tf-gw-integration]
  rest_api_id = aws_api_gateway_rest_api.tf-gw-rest-api.id
  stage_name  = "test"
}