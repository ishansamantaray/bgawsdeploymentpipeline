resource "aws_cloudwatch_log_group" "TerraformPipelineLog" {
  name = "TerraformPipelineLog"
  tags = {
    Application = "GITLAB-TERRAFORM_TEST-CLUSTER"
  }
}

resource "aws_cloudwatch_log_stream" "TerraformPipelineLogStream" {
  name           = "TerraformPipelineLogStream1"
  log_group_name = aws_cloudwatch_log_group.TerraformPipelineLog.name
}

resource "aws_cloudwatch_log_group" "TerraformPipelineLog2" {
  name = "TerraformPipelineLog2"
  tags = {
    Application = "GITLAB-TERRAFORM_TEST-CLUSTER-02"
  }
}

resource "aws_cloudwatch_log_stream" "TerraformPipelineLogStream2" {
  name           = "TerraformPipelineLogStream2"
  log_group_name = aws_cloudwatch_log_group.TerraformPipelineLog2.name
}

