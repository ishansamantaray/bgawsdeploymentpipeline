[
  {
    "name": "GITLAB-TERRAFORM_TEST-CLUSTER-APP-TASK-02",
    "image": "975403186615.dkr.ecr.us-east-2.amazonaws.com/repo1:latest",
    "cpu": 10,
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 9000,
        "hostPort": 9000
      }
    ],
    "logConfiguration": {
    "logDriver": "awslogs",
    "options": {
        "awslogs-group": "TerraformPipelineLog2",
        "awslogs-region": "us-east-2",
        "awslogs-stream-prefix": "TerraformPipelineLogStream2"
    }
  }
  }
]
